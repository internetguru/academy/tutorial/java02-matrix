import org.testng.Assert;
import org.testng.annotations.Test;

public class MatrixTest {

  @Test
  public void constructorDims() {
    Matrix mat = new Matrix(2, 3);
    Assert.assertNotNull(mat.data);
    Assert.assertEquals(mat.numRows, 2);
    Assert.assertEquals(mat.numColumns, 3);
    Assert.assertEquals(mat.data.length, 2);
    Assert.assertEquals(mat.data[0].length, 3);
    Assert.assertEquals(mat.data[1].length, 3);
    Assert.assertEquals(mat.data[0][0], 0);
    Assert.assertEquals(mat.data[0][1], 0);
    Assert.assertEquals(mat.data[0][2], 0);
    Assert.assertEquals(mat.data[1][0], 0);
    Assert.assertEquals(mat.data[1][1], 0);
    Assert.assertEquals(mat.data[1][2], 0);
  }

  @Test
  public void constructorDimsPlus() {
    Matrix mat = new Matrix(3, 2);
    Assert.assertNotNull(mat.data);
    Assert.assertEquals(mat.numRows, 3);
    Assert.assertEquals(mat.numColumns, 2);
    Assert.assertEquals(mat.data.length, 3);
    Assert.assertEquals(mat.data[0].length, 2);
    Assert.assertEquals(mat.data[1].length, 2);
    Assert.assertEquals(mat.data[2].length, 2);
    Assert.assertEquals(mat.data[0][0], 0);
    Assert.assertEquals(mat.data[0][1], 0);
    Assert.assertEquals(mat.data[1][0], 0);
    Assert.assertEquals(mat.data[1][1], 0);
    Assert.assertEquals(mat.data[2][0], 0);
    Assert.assertEquals(mat.data[2][1], 0);
  }

  @Test
  public void constructorArray() {
    Matrix mat = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    Assert.assertNotNull(mat.data);
    Assert.assertEquals(mat.numRows, 2);
    Assert.assertEquals(mat.numColumns, 3);
    Assert.assertEquals(mat.data.length, 2);
    Assert.assertEquals(mat.data[0].length, 3);
    Assert.assertEquals(mat.data[1].length, 3);
    Assert.assertEquals(mat.data[0][0], 1);
    Assert.assertEquals(mat.data[0][1], 2);
    Assert.assertEquals(mat.data[0][2], 3);
    Assert.assertEquals(mat.data[1][0], 2);
    Assert.assertEquals(mat.data[1][1], 5);
    Assert.assertEquals(mat.data[1][2], 6);
  }

  @Test
  public void constructorArrayPlus() {
    Matrix mat = new Matrix(new int[][] {{1, 2}, {3, 2}, {5, 6}});
    Assert.assertNotNull(mat.data);
    Assert.assertEquals(mat.numRows, 3);
    Assert.assertEquals(mat.numColumns, 2);
    Assert.assertEquals(mat.data.length, 3);
    Assert.assertEquals(mat.data[0].length, 2);
    Assert.assertEquals(mat.data[1].length, 2);
    Assert.assertEquals(mat.data[2].length, 2);
    Assert.assertEquals(mat.data[0][0], 1);
    Assert.assertEquals(mat.data[0][1], 2);
    Assert.assertEquals(mat.data[1][0], 3);
    Assert.assertEquals(mat.data[1][1], 2);
    Assert.assertEquals(mat.data[2][0], 5);
    Assert.assertEquals(mat.data[2][1], 6);
  }

  @Test
  public void toStringTest() {
    Matrix mat = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    String expected = "1 2 3\n2 5 6\n";
    Assert.assertEquals(mat.toString(), expected);
  }

  @Test
  public void toStringTestPlus() {
    Matrix mat = new Matrix(new int[][] {{1, 2}, {3, 2}, {5, 6}});
    String expected = "1 2\n3 2\n5 6\n";
    Assert.assertEquals(mat.toString(), expected);
  }

  @Test
  public void equalsTest() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    Matrix mat2 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    Matrix mat3 = new Matrix();
    String mat4 = "{{1, 2, 3}, {2, 5, 6}}";
    Matrix mat5 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 7}});
    Matrix mat6 = new Matrix(new int[][] {{1, 2}, {2, 5, 7}});
    Assert.assertTrue(mat1.equals(mat2));
    Assert.assertFalse(mat1.equals(mat3));
    Assert.assertFalse(mat1.equals(mat4));
    Assert.assertFalse(mat1.equals(mat5));
    Assert.assertFalse(mat1.equals(mat6));
  }

  @Test
  public void equalsTestPlus() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2}, {3, 2}, {5, 6}});
    Matrix mat2 = new Matrix(new int[][] {{1, 2}, {3, 2}, {5, 6}});
    Matrix mat3 = new Matrix();
    String mat4 = "{{1, 2, 3}, {2, 5, 6}}";
    Matrix mat5 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 7}});
    Matrix mat6 = new Matrix(new int[][] {{1, 2}, {2, 5, 7}});
    Assert.assertFalse(mat1.equals(mat6));
    Assert.assertFalse(mat1.equals(mat5));
    Assert.assertFalse(mat1.equals(mat4));
    Assert.assertFalse(mat1.equals(mat3));
    Assert.assertTrue(mat1.equals(mat2));
  }

  @Test
  public void timesTest() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    Matrix mat2 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    Matrix mat3 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}, {4, 5, 6}});
    Matrix mat4 = new Matrix();
    Matrix mat5 = mat1.times(mat3);
    Assert.assertNull(mat1.times(mat2));
    Assert.assertEquals(mat5.data.length, 2);
    Assert.assertEquals(mat5.data[0].length, 3);
    Assert.assertEquals(mat5.data[1].length, 3);
    Assert.assertEquals(mat5.data[0][0], 17);
    Assert.assertEquals(mat5.data[0][1], 27);
    Assert.assertEquals(mat5.data[0][2], 33);
    Assert.assertEquals(mat5.data[1][0], 36);
    Assert.assertEquals(mat5.data[1][1], 59);
    Assert.assertEquals(mat5.data[1][2], 72);
    Assert.assertNull(mat1.times(mat4));
  }

  @Test
  public void timesTestPlus() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2}, {3, 1}, {2, 3}});
    Matrix mat2 = new Matrix(new int[][] {{1, 2}, {3, 2}, {5, 6}});
    Matrix mat3 = new Matrix(new int[][] {{2, 5}, {6, 7}});
    Matrix mat4 = new Matrix();
    Matrix mat5 = mat1.times(mat3);
    Assert.assertEquals(mat5.data.length, 3);
    Assert.assertEquals(mat5.data[0].length, 2);
    Assert.assertEquals(mat5.data[1].length, 2);
    Assert.assertEquals(mat5.data[2].length, 2);
    Assert.assertEquals(mat5.data[0][0], 14);
    Assert.assertEquals(mat5.data[0][1], 19);
    Assert.assertEquals(mat5.data[1][0], 12);
    Assert.assertEquals(mat5.data[1][1], 22);
    Assert.assertEquals(mat5.data[2][0], 22);
    Assert.assertEquals(mat5.data[2][1], 31);
    Assert.assertNull(mat1.times(mat4));
    Assert.assertNull(mat1.times(mat2));
  }

  @Test
  public void plusTest() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    Matrix mat2 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}});
    Matrix mat3 = new Matrix(new int[][] {{1, 2}, {2, 5}, {4, 5}});
    Matrix mat4 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}, {4, 5, 6}});
    Matrix mat5 = new Matrix();
    Matrix mat6 = mat1.plus(mat2);
    Assert.assertEquals(mat6.data.length, 2);
    Assert.assertEquals(mat6.data[0].length, 3);
    Assert.assertEquals(mat6.data[1].length, 3);
    Assert.assertEquals(mat6.data[0][0], 2);
    Assert.assertEquals(mat6.data[0][1], 4);
    Assert.assertEquals(mat6.data[0][2], 6);
    Assert.assertEquals(mat6.data[1][0], 4);
    Assert.assertEquals(mat6.data[1][1], 10);
    Assert.assertEquals(mat6.data[1][2], 12);
    Assert.assertNull(mat1.plus(mat3));
    Assert.assertNull(mat3.plus(mat4));
    Assert.assertNull(mat3.plus(mat5));
 }

  @Test
  public void plusTestPlus() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2}, {3, 2}, {5, 6}});
    Matrix mat2 = new Matrix(new int[][] {{1, 2}, {3, 2}, {5, 6}});
    Matrix mat3 = new Matrix(new int[][] {{1, 2, 2}, {5, 4, 5}});
    Matrix mat4 = new Matrix(new int[][] {{1, 2, 3}, {2, 5, 6}, {4, 5, 6}});
    Matrix mat5 = new Matrix();
    Matrix mat6 = mat1.plus(mat2);
    Assert.assertNull(mat1.plus(mat3));
    Assert.assertNull(mat3.plus(mat4));
    Assert.assertNull(mat3.plus(mat5));
    Assert.assertEquals(mat6.data.length, 3);
    Assert.assertEquals(mat6.data[0].length, 2);
    Assert.assertEquals(mat6.data[1].length, 2);
    Assert.assertEquals(mat6.data[2].length, 2);
    Assert.assertEquals(mat6.data[0][0], 2);
    Assert.assertEquals(mat6.data[0][1], 4);
    Assert.assertEquals(mat6.data[1][0], 6);
    Assert.assertEquals(mat6.data[1][1], 4);
    Assert.assertEquals(mat6.data[2][0], 10);
    Assert.assertEquals(mat6.data[2][1], 12);
  }

  @Test
  public void transposeTest() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2}, {2, 5}, {4, 7}});
    Matrix mat2 = new Matrix();
    Matrix mat3 = mat1.transpose();
    Matrix mat4 = mat2.transpose();
    Assert.assertEquals(mat3.data.length, 2);
    Assert.assertEquals(mat3.data[0].length, 3);
    Assert.assertEquals(mat3.data[1].length, 3);
    Assert.assertEquals(mat3.data[0][0], 1);
    Assert.assertEquals(mat3.data[0][1], 2);
    Assert.assertEquals(mat3.data[0][2], 4);
    Assert.assertEquals(mat3.data[1][0], 2);
    Assert.assertEquals(mat3.data[1][1], 5);
    Assert.assertEquals(mat3.data[1][2], 7);
    Assert.assertEquals(mat4.data.length, 0);
  }

  @Test
  public void transposeTestPlus() {
    Matrix mat1 = new Matrix(new int[][] {{1, 2, 2}, {5, 4, 7}});
    Matrix mat2 = new Matrix();
    Matrix mat3 = mat1.transpose();
    Matrix mat4 = mat2.transpose();
    Assert.assertEquals(mat4.data.length, 0);
    Assert.assertEquals(mat3.data.length, 3);
    Assert.assertEquals(mat3.data[0].length, 2);
    Assert.assertEquals(mat3.data[1].length, 2);
    Assert.assertEquals(mat3.data[2].length, 2);
    Assert.assertEquals(mat3.data[0][0], 1);
    Assert.assertEquals(mat3.data[0][1], 5);
    Assert.assertEquals(mat3.data[1][0], 2);
    Assert.assertEquals(mat3.data[1][1], 4);
    Assert.assertEquals(mat3.data[2][0], 2);
    Assert.assertEquals(mat3.data[2][1], 7);
  }

}
